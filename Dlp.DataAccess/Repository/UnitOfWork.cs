﻿using System;
using System.Collections.Generic;
using System.Text;
using Dlp.DataAccess.Repository.Interface;
using Dlp.DataModel.Identity;
using Dlp.DataModel.Model;

namespace Dlp.DataAccess.Repository
{
	public class UnitOfWork : IDisposable, IUnitOfWork
	{
		private readonly ApplicationDbContext _context;
		private readonly IGenericRepositoryFactory _repoFactory;

		public UnitOfWork(ApplicationDbContext context, IGenericRepositoryFactory repoFactory)
		{
			_context = context;
			_repoFactory = repoFactory;
		}

		public IGenericRepository<Expression> ExpressionRepository => _repoFactory.GetGenericRepository<Expression>(_context);
		public IGenericRepository<Customer> CustomerRepository => _repoFactory.GetGenericRepository<Customer>(_context);

		public void Save()
		{
			_context.SaveChanges();
		}

		public void Dispose()
		{
			_context?.Dispose();
		}
	}
}
