﻿using System;
using System.Collections.Generic;
using System.Text;
using Dlp.DataAccess.Repository.Interface;
using Dlp.DataModel.Common;
using Dlp.DataModel.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Dlp.DataAccess.Repository
{
	public class GenericRepositoryFactory : IGenericRepositoryFactory
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly IHttpContextAccessor _httpContextAccessor;

		public GenericRepositoryFactory(UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContextAccessor)
		{
			_userManager = userManager;
			_httpContextAccessor = httpContextAccessor;
		}
		public IGenericRepository<TEntity> GetGenericRepository<TEntity>(ApplicationDbContext context) where TEntity : Entity
		{
			return new GenericRepository<TEntity>(context, _userManager, _httpContextAccessor);
		}
	}
}
