﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Dlp.DataModel.Common;

namespace Dlp.DataAccess.Repository.Interface
{
	public interface IGenericRepository<TEntity> where TEntity : Entity
	{
		TEntity GetById(Guid id);
		IEnumerable<TEntity> Get();
		IEnumerable<TEntity> GetWhere(Expression<Func<TEntity, bool>> exp);
		TEntity Create(TEntity entity);
		TEntity Update(TEntity entity);
		void Delete(TEntity entity);
	}
}
