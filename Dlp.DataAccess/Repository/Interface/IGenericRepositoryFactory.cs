﻿using System;
using System.Collections.Generic;
using System.Text;
using Dlp.DataModel.Common;

namespace Dlp.DataAccess.Repository.Interface
{
	public interface IGenericRepositoryFactory
	{

		IGenericRepository<TEntity> GetGenericRepository<TEntity>(ApplicationDbContext context) where TEntity : Entity;
	}
}
