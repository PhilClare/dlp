﻿using System;
using System.Collections.Generic;
using System.Text;
using Dlp.DataModel.Identity;
using Dlp.DataModel.Model;

namespace Dlp.DataAccess.Repository.Interface
{
	public interface IUnitOfWork
	{
		IGenericRepository<Expression> ExpressionRepository { get; }
		IGenericRepository<Customer> CustomerRepository { get; }
		void Save();
	}
}
