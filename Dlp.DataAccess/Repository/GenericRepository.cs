﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Dlp.DataAccess.Repository.Interface;
using Dlp.DataModel.Common;
using Dlp.DataModel.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Dlp.DataAccess.Repository
{
	public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : Entity
	{
		private readonly ApplicationDbContext _context;
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly IHttpContextAccessor _httpContext;

		public GenericRepository(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IHttpContextAccessor httpContext)
		{
			_context = context;
			_userManager = userManager;
			_httpContext = httpContext;
		}
		public TEntity GetById(Guid id)
		{
			return _context.Find<TEntity>(id);
		}

		public IEnumerable<TEntity> Get()
		{
			return _context.Set<TEntity>().AsEnumerable();
		}

		public IEnumerable<TEntity> GetWhere(Expression<Func<TEntity, bool>> exp)
		{
			return _context.Set<TEntity>().Where(exp).AsEnumerable();
		}

		public TEntity Create(TEntity entity)
		{
			var user = _userManager.GetUserAsync(_httpContext.HttpContext.User).Result;
			if (user != null)
			{
				entity.CreatedById = user.Id;
			}
			entity.CreatedOn = DateTime.Now;
			return _context.Add(entity).Entity;
		}

		public TEntity Update(TEntity entity)
		{
			var user = _userManager.GetUserAsync(_httpContext.HttpContext.User).Result;
			if (user != null)
			{
				entity.LastModifiedById = user.Id;
			}
			entity.LastModifiedOn = DateTime.Now;
			return _context.Update(entity).Entity;
		}

		public void Delete(TEntity entity)
		{
			_context.Remove(entity);
		}
	}
}
