﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Dlp.DataAccess.Migrations
{
    public partial class tenantEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
            migrationBuilder.DropForeignKey(
                name: "FK_Expressions_AspNetUsers_CreatedById",
                table: "Expressions");

            migrationBuilder.DropForeignKey(
                name: "FK_Expressions_AspNetUsers_LastModifiedById",
                table: "Expressions");

            migrationBuilder.DropIndex(
                name: "IX_Expressions_CreatedById",
                table: "Expressions");

	        migrationBuilder.DropIndex(
		        name: "IX_Expressions_LastModifiedById",
		        table: "Expressions");

          

            migrationBuilder.AddColumn<Guid>(
                name: "CustomerId",
                table: "Expressions",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Expressions_CustomerId",
                table: "Expressions",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Expressions_Customers_CustomerId",
                table: "Expressions",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Expressions_Customers_CustomerId",
                table: "Expressions");

            migrationBuilder.DropIndex(
                name: "IX_Expressions_CustomerId",
                table: "Expressions");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Expressions");


            migrationBuilder.CreateIndex(
                name: "IX_Expressions_CreatedById",
                table: "Expressions",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Expressions_LastModifiedById",
                table: "Expressions",
                column: "LastModifiedById");

       

            migrationBuilder.AddForeignKey(
                name: "FK_Expressions_AspNetUsers_CreatedById",
                table: "Expressions",
                column: "CreatedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Expressions_AspNetUsers_LastModifiedById",
                table: "Expressions",
                column: "LastModifiedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
