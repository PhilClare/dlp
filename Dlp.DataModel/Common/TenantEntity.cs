﻿using Dlp.DataModel.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dlp.DataModel.Common
{
	public class TenantEntity : Entity
	{
		public Guid CustomerId { get; set; }
		public virtual Customer Customer { get; set; }
	}
}
