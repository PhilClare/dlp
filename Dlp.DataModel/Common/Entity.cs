﻿using System;
using System.Collections.Generic;
using System.Text;
using Dlp.DataModel.Identity;

namespace Dlp.DataModel.Common
{
	public class Entity
	{
		public Guid Id { get; set; }
		public Guid CreatedById { get; set; }
		public DateTime CreatedOn { get; set; }
		public Guid LastModifiedById { get; set; }
		public DateTime LastModifiedOn { get; set; }

	}
}
