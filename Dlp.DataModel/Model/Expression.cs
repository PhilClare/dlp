﻿using System;
using System.Collections.Generic;
using System.Text;
using Dlp.DataModel.Common;

namespace Dlp.DataModel.Model
{
	public class Expression : TenantEntity
	{
		public string Name { get; set; }
		public string Regex { get; set; }
	}
}
