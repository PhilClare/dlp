﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Dlp.DataModel.Identity
{
	public class ApplicationUser : IdentityUser<Guid>
	{
		public Guid CustomerId { get; set; }
	}
}
