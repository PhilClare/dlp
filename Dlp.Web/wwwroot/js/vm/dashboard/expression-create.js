$(document).ready(function() {
  $(".delete-button[data-id]").click(function(e) {
    let id = this.getAttribute("data-id");
    let self = this;
    let modal = window.modal({
      actions: [
        {
          classes: ["action", "action-red"],
          text: "Yes",
          click: function() {
            $.ajax({
              url: "/api/dashboard/expressions/" + id,
              method: "DELETE",
              success: function() {
                modal.close();
                $(self)
                  .parents("section")
                  .remove();
              }
            });
          }
        },
        {
          classes: ["action"],
          text: "No",
          click: function() {
            modal.close();
          }
        }
      ],
      text: "Are you sure you want to remove this expression"
    });
    modal.open();
  });
});
