﻿window.modal = function(options) {
  this.wrapper = $(document.createElement("div"));
  this.wrapper.hide();
  this.wrapper.addClass("modal");
  this.wrapper.addClass("section");
  this.wrapper.append("<p>" + options.text + "</p><hr/>");
  let self = this;
  if (options.actions) {
    for (const action of options.actions) {
      if (action) {
        let button = $(document.createElement("a"));
        button.html(action.text);
        if (action.classes) {
          for (const cls of action.classes) {
            button.addClass(cls);
          }
        }
        button.click(action.click);
        self.wrapper.append(button);
      }
    }
    $("body").append(self.wrapper);
  }
  self.open = function() {
    self.wrapper.show();
  };
  self.close = function() {
    self.wrapper.hide();
  };
  return self;
};
