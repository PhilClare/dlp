﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dlp.DataModel.Identity;
using Dlp.DataModel.Model;
using Dlp.Web.Areas.Dashboard.Models.Expressions;

namespace Dlp.Web.Areas.Dashboard.ModelMaps
{
	public static class ExpressionModelMaps
	{
		public static ExpressionViewModel ToExpressionViewModel(this Expression entity, IEnumerable<ApplicationUser> users)
		{
			return new ExpressionViewModel
			{ 
				Name = entity.Name,
				Expression = entity.Regex,
				CreatedById = entity.CreatedById,
				CreatedOn = entity.CreatedOn,
				CreatedBy = users.FirstOrDefault(x => x.Id == entity.CreatedById).UserName,
				Id = entity.Id
			};
		}
		public static ExpressionPostViewModel ToExpressionPostViewModel(this Expression entity)
		{
			return new ExpressionPostViewModel
			{
				Name = entity.Name,
				Expression = entity.Regex,
			};
		}

		public static Expression ToExpression(this ExpressionPostViewModel model, ApplicationUser user)
		{
			return new Expression
			{
				Id = Guid.NewGuid(),
				Name = model.Name,
				Regex = model.Expression,
				CustomerId = user.CustomerId
			};
		}
	}
}
