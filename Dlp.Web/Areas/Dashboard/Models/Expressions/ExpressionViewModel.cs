﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dlp.Web.Areas.Dashboard.Models.Expressions
{
	public class ExpressionViewModel
	{
		public Guid Id { get; set; }
		public string Expression { get; set; }
		public string Name { get; set; }
		public string CreatedBy { get; set; }
		public Guid CreatedById { get; set; }
		public DateTime CreatedOn { get; set; }

	}
}
