﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dlp.Web.Areas.Dashboard.Models.Expressions
{
	public class ExpressionPostViewModel
	{
		[DisplayName("Expression")]
		[Required]
		public string Expression { get; set; }
		[Required]
		[DisplayName("Name")]
		public string Name { get; set; }
	}
}
