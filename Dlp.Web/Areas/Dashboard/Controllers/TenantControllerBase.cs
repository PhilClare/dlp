﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dlp.DataModel.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Dlp.Web.Areas.Dashboard.Controllers
{
	public class TenantControllerBase : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;

		public TenantControllerBase(UserManager<ApplicationUser> userManager)
		{
			_userManager = userManager;
		}

		protected ApplicationUser CurrentUser { get; set; }
		public override void OnActionExecuting(ActionExecutingContext context)
		{
			CurrentUser = _userManager.GetUserAsync(User).Result;
			base.OnActionExecuting(context);
		}
	}
}
