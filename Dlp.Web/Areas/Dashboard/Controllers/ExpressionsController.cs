﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dlp.DataAccess.Repository.Interface;
using Dlp.DataModel.Identity;
using Dlp.Web.Areas.Dashboard.ModelMaps;
using Dlp.Web.Areas.Dashboard.Models.Expressions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Dlp.Web.Areas.Dashboard.Controllers
{
	[Area("Dashboard")]
	[Route("/dashboard/expressions")]
	[Authorize]
    public class ExpressionsController : TenantControllerBase
    {
	    private readonly IUnitOfWork _unitOfWork;
	    private readonly UserManager<ApplicationUser> _userManager;

	    public ExpressionsController(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager) : base(userManager)
	    {
		    _unitOfWork = unitOfWork;
		    _userManager = userManager;
	    }
        public IActionResult Index()
        {
	        var expressions = _unitOfWork.ExpressionRepository.GetWhere(x => x.CustomerId == CurrentUser.CustomerId);
			var users = _userManager.Users.Where(x => x.CustomerId == CurrentUser.CustomerId);
	        var models = expressions.Select(x => x.ToExpressionViewModel(users)).ToArray();
			
            return View(models);
        }
		[Route("create")]
	    public IActionResult Create()
	    {
		    return View();
	    }

		[HttpPost]
		[Route("create")]
		public IActionResult Create(ExpressionPostViewModel model)
	    {
		    if (!ModelState.IsValid)
		    {
			    return View(model);
		    }

		    _unitOfWork.ExpressionRepository.Create(model.ToExpression(CurrentUser));
			_unitOfWork.Save();
		    return RedirectToAction("Index");
	    }
	    [Route("edit/{id}")]
	    public IActionResult Edit(Guid id)
	    {
		    var entity = _unitOfWork.ExpressionRepository.GetById(id);
		    if (entity == null)
		    {
			    return NotFound();
		    }
		    var users = _userManager.Users.Where(x => x.CustomerId == CurrentUser.CustomerId);
			var model = entity.ToExpressionPostViewModel();
		    ViewData["Id"] = id;
		    return View(model);
	    }

	    [HttpPost]
	    [Route("edit/{id}")]
	    public IActionResult Edit(Guid id, ExpressionPostViewModel model)
	    {
		    if (!ModelState.IsValid)
		    {
			    return View(model);
		    }

		    var entity = _unitOfWork.ExpressionRepository.GetById(id);

		    if (entity == null || entity.CustomerId != CurrentUser.CustomerId)
		    {
			    return View(model);
			}

		    entity.Name = model.Name;
		    entity.Regex = model.Expression;

		    _unitOfWork.ExpressionRepository.Update(entity);
		    _unitOfWork.Save();

		    return RedirectToAction("Index");
	    }
	}
}