﻿using Dlp.DataAccess.Repository.Interface;
using Dlp.DataModel.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Dlp.Web.Areas.Dashboard.Controllers.Api
{
	[Route("api/dashboard/expressions")]
	[ApiController]
	[Authorize]
	public class ExpressionsApiController : TenantControllerBase
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly IUnitOfWork _unitOfWork;

		public ExpressionsApiController(UserManager<ApplicationUser> userManager, IUnitOfWork unitOfWork) : base(userManager)
		{
			_userManager = userManager;
			_unitOfWork = unitOfWork;
		}

		[HttpDelete("{id}")]
		public ActionResult Delete([FromRoute]Guid id)
		{
			var expression = _unitOfWork.ExpressionRepository.GetById(id);
			if (expression.CustomerId != CurrentUser.CustomerId)
			{
				return BadRequest();
			}

			_unitOfWork.ExpressionRepository.Delete(expression);
			_unitOfWork.Save();

			return NoContent();
		}
	}
}